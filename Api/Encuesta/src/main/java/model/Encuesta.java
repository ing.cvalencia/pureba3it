package model;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "encuestas")
@Data
@CompoundIndexes({
    @CompoundIndex(name = "mail_idx", def = "{'email': 1}")
})
public class Encuesta {
	@Id
	 private String email;   
	 private String opcion;
	    
}

package repo;

import model.Encuesta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EncuestaRepository extends MongoRepository<Encuesta, String> {
    
}

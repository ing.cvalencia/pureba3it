package controller;

import model.Encuesta;
import repo.EncuestaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/encuestas")
public class EncuestaController {

	@Autowired
    private EncuestaRepository encuestaRepository;
    
    @GetMapping("/")
    List<Encuesta> index(){
    	return encuestaRepository.aggregate([
		{
        $group: 
        {
            _id: "$opcion",
            count: { $sum: 1 }
        }
		}
		])	
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    Encuesta create(@RequestBody Encuesta encuesta){
        return encuestaRepository.save(encuesta);
    }
}

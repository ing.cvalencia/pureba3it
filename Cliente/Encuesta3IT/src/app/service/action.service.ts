import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_BASE = "http://localhost:8080";

@Injectable({
  providedIn: 'root'
})

export class ActionService {

  constructor(private http: HttpClient) {
    
  };

getResult(){
  return this.http.get(API_BASE + "/encuestas");
}


ingrearRespuesta(encuesta: any){
  return this.http.post(API_BASE + "/encuestas", encuesta);
}


}

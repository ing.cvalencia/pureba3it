import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from 'src/app/components/menu/menu.component';
import { EncuestaComponent } from 'src/app/components/encuesta/encuesta.component';
import { AcercaDeComponent } from '../../components/acerca-de/acerca-de.component';
import { ResultadosComponent } from '../../components/resultados/resultados.component';



const APP_ROUTER : Routes = [
  {path: '', redirectTo: 'menu', pathMatch: 'full'},
  {path: 'resultados', component: ResultadosComponent,  data:  { bodyClass: 'yellow' }},
  {path: 'acercade', component: AcercaDeComponent,  data:  { bodyClass: 'grey' }},
  {path: 'encuesta', component: EncuestaComponent,  data:  { bodyClass: 'purple' }},
  {path: 'menu', component: MenuComponent,  data:  { bodyClass: 'grey' }},
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTER)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

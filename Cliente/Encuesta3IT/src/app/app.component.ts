import { Component, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterOutlet } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { filter, map, mergeMap } from 'rxjs/operators';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Encuesta 3IT';

  constructor(
    @Inject(DOCUMENT) private document : any, 
    private renderer: Renderer2, 
    private router: Router, 
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .pipe(map(() => this.activatedRoute))
      .pipe(map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
      .pipe(filter((route) => route.outlet === 'primary'))
      .pipe(mergeMap((route) => route.data))
      .subscribe((data) => this.updateBodyClass(data.bodyClass));
  }

  private updateBodyClass(customBodyClass?: string) {
  
    this.renderer.setAttribute(this.document?.body, 'class', '');
    this.renderer.setAttribute(this.document?.querySelector('.mat-toolbar'),'class','mat-toolbar mat-toolbar-single-row');
    if (customBodyClass) {
      this.renderer.addClass(this.document?.body, customBodyClass);
      this.renderer.addClass(this.document?.querySelector('.mat-toolbar'), customBodyClass);
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import LinearGradient from 'zrender/lib/graphic/LinearGradient';
import { ActionService } from '../../service/action.service';


@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})
export class ResultadosComponent implements OnInit {
  
  options: any;

  constructor(private actionServices: ActionService) { }

  ngOnInit(): void {
    const dataAxis: any= [];
    const data: any= [];
    const yMax = 500;
    const dataShadow = [];
    const encuestas: any = [];

    this.actionServices.getResult()
    .subscribe(encuestas);

    encuestas.forEach(function (e:any) {
      dataAxis.push(e.opcion);
      data.push(e.count);
    }); 

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < data.length; i++) {
      dataShadow.push(yMax);
    }

    this.options = {
      title: {
        text: "Resultados",
        left: "center",
 
        textStyle: {
          color:"white"  ,
          fontSize: 30
        },
        
      },
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "shadow"
        }
      },
      xAxis: {
        data: ["rock","pop","meta"],
        axisLabel: {
          color: '#fff',
        },
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false,
        },
        z: 10,
      },
      yAxis: {
        axisLine: {
          show: false,
        },
        axisTick: {
          show: false,
        },
        axisLabel: {
          textStyle: {
            color: '#fff',
          },
        },
      },
      dataZoom: [
        {
          type: 'inside',
        },
      ],
      series: [
        {
          // For shadow
          type: 'bar',
          itemStyle: {
            color: 'rgba(0,0,0,0.05)'
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: dataShadow,
          animation: false,
        },
        {
          type: 'bar',
          itemStyle: {
            color: new LinearGradient(0, 0, 0, 1, [
              { offset: 0, color: '#83bff6' },
              { offset: 0.5, color: '#188df0' },
              { offset: 1, color: '#188df0' },
            ]),
          },
          emphasis: {
            itemStyle: {
              color: new LinearGradient(0, 0, 0, 1, [
                { offset: 0, color: '#2378f7' },
                { offset: 0.7, color: '#2378f7' },
                { offset: 1, color: '#83bff6' },
              ]),
            }
          },
         data: [15,20,40],
        },
      ],
    };
  }

  onChartEvent(event: any, type: string) {
    console.log('chart event:', type, event);
  }
}

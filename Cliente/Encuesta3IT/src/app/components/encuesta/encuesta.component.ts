import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/service/shared.service';
import { ActionService } from '../../service/action.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

interface Estilo {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})

export class EncuestaComponent implements OnInit {

  estilos: Estilo[] = [
    {value: 'Rock', viewValue: 'Rock'},
    {value: 'Metal', viewValue: 'Metal'},
    {value: 'Clasica', viewValue: 'Clasica'},
    {value: 'Pop', viewValue: 'Pop'},
  ];

   encuesta = {
     opcion : '', 
     email :  ''
   }
  
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  matcher = new MyErrorStateMatcher();

  constructor(private sharedService:SharedService, private actionServices:ActionService, private router:Router) { 
    this.sharedService.sendClickEvent();
  }

  ngOnInit(): void {
    Swal.fire('Encuesta recibida exitosamente');        
        this.router.navigate(['/resultados']);

     ;
  }

   create(){
     this.actionServices.ingrearRespuesta(this.encuesta)
     .subscribe(()=>{
      
     this.encuesta = {
      opcion : '', 
      email :  ''
     }  
      Swal.fire('Encuesta recibida exitosamente');        
        this.router.navigate(['/resultados']);

     })
   } 


}
